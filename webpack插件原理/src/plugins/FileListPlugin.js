let fs = require('fs')
module.exports = class FileListPlugin {
  constructor({ filename }) {
    this.filename = filename
  }
  apply(compiler) {
    // 文件已经准备好了
    compiler.hooks.emit.tapAsync('FileListPlugin', (compilcation, cb) => {
      let assets = compilcation.assets
      let content = `## 文件名    资源大小`
      Object.entries(assets).forEach(([filename, statObj]) => {
        content += `\r\n- ${filename}    ${statObj.size()}`
      })
      // 在assets中添加一个文件
      assets[this.filename] = {
        source() {
          return content
        },
        size() {
          return content.length
        }
      }
      // console.log(compilcation.assets)
      cb()
    })
  }
}