class AsyncPlugin {
  apply(compiler) {
    compiler.hooks.emit.tapAsync('AsyncPlugin', (compilcation, cb) => {
      console.log('等5秒 发射文件')
      setTimeout(() => {
        cb()
      }, 5000)
    })

    compiler.hooks.emit.tapPromise('AsyncPlugin', (compilcation) => {
      return new Promise((resolve, reject) => {
        console.log('再等2秒')
        setTimeout(() => {
          resolve()
        }, 2000)
      })
    })
  }
}
module.exports = AsyncPlugin
