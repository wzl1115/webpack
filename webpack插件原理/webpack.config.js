const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const DonePlugin = require('./src/plugins/donePlugin')
const AsyncPlugin = require('./src/plugins/AsyncPlugin')
const FileListPlugin = require('./src/plugins/FileListPlugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.[hash].js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['js', 'less', 'css']
  },
  module: {
    rules: [
      {
        test: /\.css/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              plulicPath: '../'
            }
          },
          'css-loader'
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template:  './src/idnex.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'css/main.[hash].css',
    }),
    new CleanWebpackPlugin(),
    // new DonePlugin(),
    // new AsyncPlugin(),
    new FileListPlugin({
      filename: 'list.md'
    })
  ]
}