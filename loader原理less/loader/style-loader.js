function loader (source) {
  // source 需要去除换行  JSON.stringify
  let style = `
    let style = document.createElement('style')
    style.innerHTML = ${JSON.stringify(source)}
    document.head.appendChild(style)
  `
  return style
}
module.exports = loader