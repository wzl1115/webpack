// 用自己写的打包工具测试  wzl-pack
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
class P {
  apply(compiler) {
    compiler.hooks.emit.tap('emit', function () {
      console.log('emit')
    })
  }
}
// import webpack from 'webpack'
module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist/lib')
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          path.resolve(__dirname, './loader/style-loader.js'),
          path.resolve(__dirname, './loader/less-loader.js')
        ]
      }
    ]
  },
  plugins: [
    // new HtmlWebpackPlugin({
    //   template: './src/index.html'
    // }),
    new P()
  ]
}