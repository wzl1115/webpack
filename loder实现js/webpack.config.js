const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'source-map',
  watch: true,
  resolveLoader: {
    // 指定去那个路径寻找loader，node_modules中找不到，进去./src/loader中找
    modules: ['node_modules', path.resolve(__dirname, './src/loader')],
    // // 设置loader别名
    // alias: {
    //   loader1: path.resolve(__dirname, './src/loader/loader1')
    // }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env'
              ]
            }
          },
          {
            loader: 'banner-loader',
            options: {
              text: '王志磊',
              path: path.resolve(__dirname, './src/banner.js')
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
}