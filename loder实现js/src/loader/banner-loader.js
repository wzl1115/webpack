let loaderUtils = require('loader-utils')
let validateOptions = require('schema-utils')
let fs = require('fs')
module.exports = function (source) {
  // this.cacheable(false) // 每次打包不需要缓存
  let options = loaderUtils.getOptions(this)
  let cb = this.async()
  let schame = {
    type: 'object',
    properties: {
      text: {
        type: 'string'
      },
      path: {
        type: 'string'
      }
    }
  }
  validateOptions(schame, options, 'banner-loader')
  if (options.path) {
    this.addDependency(options.path) // 将当前文件添加到依赖  热更新 watch时才能动态更新
    fs.readFile(options.path, 'utf8', function (err, resut) {
      cb(err, `/** ${resut} */ ${source}`)
    })
  } else cb(null, `/** ${options.text} */ ${source}`)
}