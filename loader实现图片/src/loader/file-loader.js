const loaderUtils = require('loader-utils')
function loader (source) {
  // 生成图片路径
  let filename = loaderUtils.interpolateName(this, '[hash].[ext]', {
    content: source
  })
  this.emitFile(filename, source)
  return `module.exports = '${filename}'`
}
loader.raw = true // 二进制
module.exports = loader