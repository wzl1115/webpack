const loaderUtils = require('loader-utils')
const mime = require('mime')
const fileLoader = require('./file-loader')
function loader(source) {
  let options = loaderUtils.getOptions(this)
  if ( options && source.length < options.limit) {
    return `module.exports = 'data:${mime.getType(this.resourcePath)};base64,${source.toString('base64')}'`
  } else {
    // let filename = loaderUtils.interpolateName(this, '[hash].[ext]', {
    //   content: source
    // })
    // this.emitFile(filename, source)
    // return `module.exports = '${filename}'`
    return fileLoader.call(this, source)
  }
}
loader.raw = true // 二进制
module.exports = loader