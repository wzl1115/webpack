const path = require('path')

// 打包实现的源码在wzl-pack中，如何使用见wzl-pack/bin/wzl-pack.js 终端输入wzl-pack即可
module.exports = {
  mode: 'development',
  entry: './src/index.js' ,
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  }
}