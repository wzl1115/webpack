class SyncHook {
  constructor () {
    this.funs = []
  }
  tap (fun) {
    this.funs.push(fun)
  }
  call () {
    this.funs.forEach(fun => {
      fun(...arguments)
    })
  }
}

class Lesson {
  constructor () {
    this.hooks = {
      arch: new SyncHook(['name'])
    }
  }
  tap () {
    // 注册监听函数
    this.hooks.arch.tap(function (name) {
      console.log('node', name)
    })
    this.hooks.arch.tap(function (name) {
      console.log('react', name)
    })
  }
  start () {
    this.hooks.arch.call('wzl')
  }
}

let lesson = new Lesson()
lesson.tap() // 注册事件
lesson.start() // 启动钩子