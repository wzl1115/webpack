#! /usr/bin/env node
// 外部需要使用命令，需要建立bin文件夹，同时在package.json中添加 "bin": "./bin/wzl-pack.js"
// 1.首行 '#! /usr/bin/env node' 指定用node运行
// 2.使用npm link 将当前包连接到全局
// 3.需要使用当前模块的项目 使用npm link wzl-pack建立连接 （wzl-pack是package.json的name） 就可以使用wzl-pack命令了

// 1) 需要找到当前执行名的路径， 拿到webpack.config.js
const path = require('path')
let config = require(path.resolve('webpack.config.js'))

let Compiler = require('../lib/compiler')
let compiler = new Compiler(config)
compiler.hooks.entryOption.call()
// 运行编译
compiler.run()
