const path =require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'mian.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolveLoader: {
    modules: ['node_modules', path.resolve(__dirname, './src/loader')]
  },
  module: {
    rules: [
      {
        test: /\.(npg|jpg)$/,
        use: [
          // 'file-loader',
          {
            loader: 'url-loader',
            options: {
              limit: 100 * 1024
            }
          }
        ]
      },
      {
        test: /\.(css|less)$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
}