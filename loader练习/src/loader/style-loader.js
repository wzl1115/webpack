let loaderUtils = require('loader-utils')
function loader(source) {
  let str = `
    let style = document.createElement('style')
    style.innerHTML = ${JSON.stringify(source)}
    document.body.appendChild(style)
  `
  console.log(1211111)
  return str
}
// 在style-loader上写了pitch方法之后 不会执行自己loader中得方法了
// remainingRequest 是剩余未得请求 ../loader/css-loader.js!./index.css //行内loader
// 上面行内loader得意思是把index.css文件交给css-loader来处理
// 最终我们导出得时候会在前面加上!!, 表示什么loader都不要，只交给行类loader来处理
// -！表示不会让文件 再去通过（pre: 前置 normal:普通的） 这两类loader处理， post表示后置loader 最后执行
loader.pitch = function (remainingRequest) {
  // 让style-loader去处理剩余得请求remainingRequest
  // console.log(loaderUtils.stringifyRequest(this, remainingRequest), 1111111111111111111111)
  let str = `
  let style = document.createElement('style')
  style.innerHTML = require(${loaderUtils.stringifyRequest(this, '!!' + remainingRequest)})
  document.body.appendChild(style)
  `
  return str
}
module.exports = loader
